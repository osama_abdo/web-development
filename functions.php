<?php

add_action( 'wp_enqueue_scripts', function(){

    wp_enqueue_style( 'os-style', get_template_directory_uri() . '/css/os_Style.css' );

    wp_enqueue_script( 'jquery', false );

    wp_enqueue_style( 'main-style', 'https://entrepcoaches.com/wp-content/themes/stevepreda/style.css' );
} );

add_theme_support( 'post-thumbnails' );

show_admin_bar( 0 );