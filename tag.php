<?php get_header('has-img') ?>


<main>
    <section>
        <div class="grid-container">
            <h2><?php single_cat_title() ?></h2>
            <?php $counter = 1 ?>
            <?php while (have_posts()) : the_post() ?>
                <div class="col" style="<?php if(wp_is_mobile()){echo 'clear:none';} ?>">
                <div class="img">
                    <a href="<?php the_permalink() ?>">
                        <?php if(has_post_thumbnail(  )) : ?>
                            <img src="<?php the_post_thumbnail_url('large') ?>"	 />
                        <?php else: ?>
                            <img style="max-height: 264px;" src="<?php echo get_template_directory_uri() . '/img/no-image.gif' ?>" />
                        <?php endif; ?>
                    </a>
                    <p>
                        Posted in <?php single_cat_title() ?>
                    </p>
                </div>
                <p class="date">
	                <?php the_time('M d, Y') ?>
                </p><h3><?php the_title() ?></h3>
                <p>
                    <a href="<?php the_permalink() ?>">Continue Reading</a>
                </p>
            </div>
            <?php if ( !wp_is_mobile() && $counter % 6 == 0 ){echo '<div></div>';}elseif(wp_is_mobile() && $counter % 2 == 0){echo '<div style="clear:both"></div>';} ?>                            
            <?php $counter++; endwhile; ?>
        </div>
    </section>
</main>

</body>
</html>

<?php get_footer() ?>