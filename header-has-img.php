<!doctype html><!--[if lt IE 7]><html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>News Archives - Continental Underwriters, Inc.</title>
    <link rel="stylesheet" href="https://use.typekit.net/ocm3nsw.css">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php wp_head() ?>

<script type="text/javascript">
    (function($) {
        $(window).scroll(function(){
            if($(this).scrollTop() > 300){
                $('header').addClass('fixed')
            }else{
                $('header').removeClass('fixed')
            }
        });
    })( jQuery );
</script>
</head>
<?php
$body_class= '';
if(is_page( 'blog' )){
    $body_class = 'blog  et_pb_pagebuilder_layout et_fixed_nav et_pb_footer_columns2 et_pb_gutters3';
}elseif(is_tag()){
    $body_class = 'category et_pb_pagebuilder_layout et_fixed_nav et_pb_footer_columns2 et_pb_gutters3';
}else{
    $body_class = 'et_pb_pagebuilder_layout et_fixed_nav et_pb_footer_columns2 et_pb_gutters3';
}
?>
<body <?php body_class( $body_class ) ?> itemscope itemtype="http://schema.org/WebPage">


<header id="main-header" data-height-onload="99" data-height-loaded="true" data-fixed-height-onload="75" class="" style="top: 0px;padding:20px 0">

    <div class="container clearfix et_menu_container">

        <div class="logo_container">

            <span class="logo_helper"></span>

            <a href="https://entrepcoaches.com/">

                

                <img src="https://entrepcoaches.com/wp-content/uploads/2017/11/walt-brown-logo-white-bg.png" alt="steve Preda - Certified EOS® Implementer in Raleigh, NC" id="logo" data-height-percentage="60" data-actual-width="106" data-actual-height="72">

            </a>

        </div>

        <div id="et-top-navigation" data-height="63" data-fixed-height="40" style="padding-left: 110px;">

            <nav id="top-menu-nav">

                <ul id="top-menu" class="nav"><li id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item"><a href="https://entrepcoaches.com/"><span class="">Home</span></a></li>
    <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/steve-preda/"><span class="">Steve Preda</span></a></li>
    <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/what-is-eos/"><span class="">What is EOS?</span></a></li>
    <li id="menu-item-19" class="arrowsign menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/getting-started/"><span class="et_pb_more_button et_pb_button">Get Started</span></a></li>
    <li id="menu-item-201" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/eos-blog/"><span class="">Steve’s EOS® Blog</span></a></li>
    </ul>
            </nav>





            <div id="et_mobile_nav_menu">

                <div class="mobile_nav closed">

                    <span class="select_page">Select Page</span>

                    <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>

                    
                    <ul id="mobile_menu" class="et_mobile_menu"><li id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item et_first_mobile_item"><a href="https://entrepcoaches.com/"><span class="">Home</span></a></li>
    <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/steve-preda/"><span class="">Steve Preda</span></a></li>
    <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/what-is-eos/"><span class="">What is EOS?</span></a></li>
    <li id="menu-item-19" class="arrowsign menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/getting-started/"><span class="et_pb_more_button et_pb_button">Get Started</span></a></li>
    <li id="menu-item-201" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/eos-blog/"><span class="">Steve’s EOS® Blog</span></a></li>
    </ul>
                <ul id="mobile_menu" class="et_mobile_menu"><li id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item et_first_mobile_item"><a href="https://entrepcoaches.com/"><span class="">Home</span></a></li>
    <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/steve-preda/"><span class="">Steve Preda</span></a></li>
    <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/what-is-eos/"><span class="">What is EOS?</span></a></li>
    <li id="menu-item-19" class="arrowsign menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/getting-started/"><span class="et_pb_more_button et_pb_button">Get Started</span></a></li>
    <li id="menu-item-201" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://entrepcoaches.com/eos-blog/"><span class="">Steve’s EOS® Blog</span></a></li>
    </ul></div>

            </div>

        </div>

        <!-- #et-top-navigation -->

    </div>

    <!-- .container -->

    <div class="et_search_outer">

        <div class="container et_search_form_container">

            <form role="search" method="get" class="et-search-form" action="https://mydemoserver.site/stevepreda/">

                <input type="search" class="et-search-field" placeholder="Search …" value="" name="s" title="Search for:"> </form>

            <span class="et_close_search_field"></span>

        </div>

    </div>

</header>



<?php if(is_category()) : ?>
    <div class="hero"style="background-image: url(<?php echo(!empty(get_field('category_header_image',get_queried_object())) ? get_field('category_header_image',get_queried_object()) : 'https://images3.alphacoders.com/853/thumb-1920-85305.jpg' )  ?>);">
        <div class="grid-container">
            <h1><a href="#" rel="category tag"><?php single_cat_title() ?></a></h1>
        </div>
    </div>
<?php endif; ?>

<?php if(is_page()) : ?>
    <div class="hero"style="background-image: url(<?php echo(!empty(get_field('category_header_image',get_queried_object())) ? get_field('blog_header_image',get_queried_object()) : 'https://images3.alphacoders.com/853/thumb-1920-85305.jpg' )  ?>);">
        <div class="grid-container">
            <h1><a href="#" rel="category tag" style="color:white;text-decoration:none"><?php the_title() ?></a></h1>
        </div>
    </div>
<?php endif; ?>