<?php get_header(  ) ?>
<main>
    <section>
        <div class="grid-container">
        <?php
        while(have_posts()) : the_post(); ?>
            <article>
                <img src="<?php the_post_thumbnail_url( 'large' ) ?>" class="fImg" /><h1><?php the_title() ?></h1>
                <?php the_content(  ) ?>
                <?php if(has_tag()) : ?>
                    <div class="tags">
                        <?php the_tags() ?>
                    </div>
                <?php endif; ?>

                <div class="navigation">
                    <?php previous_post_link('%link', '&#8592; %title'); ?>
                    <?php next_post_link('%link', '%title &#8594;'); ?>
                </div>
             
            </article>
            
        <?php endwhile ?>
        </div>
    </section>
</main>

<?php get_footer(  ) ?>
