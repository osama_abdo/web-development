
<footer id="main-footer" style="padding:0">



<div class="container" style='width:100%'>

    <div id="footer-widgets" class="clearfix">

                    <div class="footer-widget" style="width: 45.25%;margin: 0 2.5% 5.5% 0;">

                        <div id="custom_html-2" class="widget_text fwidget et_pb_widget widget_custom_html">

                            <div class="textwidget custom-html-widget">

                                
                                                        
                                                                <div class="textwidget"><p><iframe src="https://www.youtube.com/embed/RTEL_yvin3Q?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe></p>
                        <h3 style="color: white; font-weight: bold; font-size: 28px; text-align: center;">Contact Us</h3>
                        <h3 style="color: white; font-size: 18px; margin-bottom: 10px;">To discuss if implementing EOS / Traction would be a good fit for your needs.</h3>
                        <p><!--[contact-form-7 id="934" title="Pop-up Form_copy"]--></p>
                        <div class="foo-fr">
                        <div role="form" class="wpcf7" id="wpcf7-f934-o2" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="/#wpcf7-f934-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                        <div style="display: none;">
                        <input type="hidden" name="_wpcf7" value="934"><br>
                        <input type="hidden" name="_wpcf7_version" value="4.9.2"><br>
                        <input type="hidden" name="_wpcf7_locale" value="en_US"><br>
                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f934-o2"><br>
                        <input type="hidden" name="_wpcf7_container_post" value="0">
                        </div>
                        <p><span class="wpcf7-form-control-wrap text-771"><input type="text" name="text-771" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required popup_first" aria-required="true" aria-invalid="false" placeholder="First and Last Name"></span><br>
                        <span class="wpcf7-form-control-wrap email-165"><input type="email" name="email-165" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email popup_email" aria-required="true" aria-invalid="false" placeholder="Email Address"></span><br>
                        <span class="wpcf7-form-control-wrap tel-899"><input type="tel" name="tel-899" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel popup_email" aria-invalid="false" placeholder="Phone"></span><br>
                        <span class="wpcf7-form-control-wrap text-771"><input type="text" name="text-771" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required popup_first" aria-required="true" aria-invalid="false" placeholder="Zip Code"></span><br>
                        <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit popup_submit"><span class="ajax-loader"></span></p>
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                        </div>
                        </div>
                        </div>
                                        
                                
                            </div>

                        </div>

            <!-- end .fwidget -->

                                        </div>

        <!-- end .footer-widget -->

        <div class="footer-widget" style="width: 45.25%;margin: 0 2.5% 5.5% 0;">

            <div id="text-2" class="fwidget et_pb_widget widget_text">

                                        
                                        <div class="textwidget"><h3 style="font-size: 18px; font-weight: 500; color: #ffffff;"><strong>Steve Preda, EOS® Implementer</strong></h3>
            <div class="steve-desc">
            <p>After corporate careers as a CPA and banking, I started an investment bank in Central Eastern Europe and ran it for 11 years. We sold scores of companies, but in many cases I felt that the owners could have built more value if they, or us, knew how to take their business to the next level, without bringing professional investors on board.&nbsp; Consequently, some of our clients sold out early and left money on the table.</p>
            <p>Since stumbling across Traction and discovering the Entrepreneurial Operating System in late 2015, my mission has been helping companies get to the next level with their own resources… Without the help of investors, high-priced consultants, or the hiring of seasoned executives.</p>
            <p>If you would like to learn how to get your business unstuck in 90 days or less and triple your profit and triple your time off within 3 years, schedule a 15-minute call with me.&nbsp; Alternatively, let’s get your team around the table for a 90-minute meeting to explore if your organization is ready to implement EOS.</p>
            </div>
            </div>
                            <div class="textwidget"><p><script type="application/ld+json">
            {"@context": "http://schema.org","@type": "LocalBusiness","name": "Steve Preda Entrep Coaching","image": "https://entrepcoaches.com/wp-content/uploads/2017/11/walt-brown-logo-white-bg.png","@id": "","url": "https://entrepcoaches.com/","telephone": "804.332.1307","address": {"@type": "PostalAddress","streetAddress": "4870 Sadler Rd. STE 300","addressLocality": "Glen Allen","addressRegion": "VA","postalCode": "23060","addressCountry": "US"},"geo": {"@type": "GeoCoordinates","latitude": 37.667531,"longitude": -77.578523},"sameAs": "https://www.linkedin.com/in/ipreda/"}
            </script></p>
            <h3 style="font-size: 18px; font-weight: 500; color: #ffffff;"><strong>Contact</strong></h3>
            <p>Steve Preda Entrep Coaching</p>
            <p>DC / MD / Northern VA Office:<br>
            Phone: (202) 508-8247<br>
            Address: <a href="https://goo.gl/zFrrCs">1629 K ST. Suite 300, Washington D.C., 20006</a></p>
            <p>Central VA Office:<br>
            Phone: (804) 332-1307<br>
            Address: <a href="https://goo.gl/QNPu35">4870 Sadler Rd. STE 300, Glen Allen, VA 23060</a></p>
            <div class="email_title">Email: <img src="https://entrepcoaches.com/wp-content/uploads/2018/01/steve-preda010016.png" alt="email"></div>
            <p>Linkedin: <a href="https://www.linkedin.com/in/ipreda/">View Steve’s Profile</a></p>
            </div>
                <a href="https://www.facebook.com/SPECRVA"><img width="48" height="48" src="https://entrepcoaches.com/wp-content/uploads/2018/01/if_Facebook_Social-Network-Communicate-Page-Curl-Effect-Circle-Glossy-Shadow-Shine_437930.png" class="image wp-image-814  attachment-full size-full" alt="Facebook Icon" style="max-width: 100%; height: auto;"></a><a href="https://twitter.com/@EntrepCoaches"><img width="48" height="48" src="https://entrepcoaches.com/wp-content/uploads/2018/01/Twitter.png" class="image wp-image-858  attachment-full size-full" alt="Twitter Icon" style="max-width: 100%; height: auto;"></a><a href="https://www.instagram.com/stevepreda/"><img width="48" height="47" src="https://entrepcoaches.com/wp-content/uploads/2018/01/insta.png" class="image wp-image-857  attachment-full size-full" alt="insta" style="max-width: 100%; height: auto;"></a><a href="https://itunes.apple.com/us/podcast/succession-secrets/id1115741201?mt=2"><img width="48" height="48" src="https://entrepcoaches.com/wp-content/uploads/2018/01/apple.png" class="image wp-image-856  attachment-full size-full" alt="apple" style="max-width: 100%; height: auto;"></a>                        
                            
                        </div>

                        <!-- end .fwidget -->

                    </div>

                    <!-- end .footer-widget -->

                </div>

    <!-- #footer-widgets -->

</div>

<!-- .container -->





<div id="footer-bottom">

    <div class="container clearfix">

        <div id="footer-info">© 2017 Steve Preda Co. All rights reserved. EOS® logos and process are owned by <a href="https://www.eosworldwide.com/">EOS Worldwide</a>.
        </div>

    </div>

    <!-- .container -->

</div>

</footer>
<?php wp_footer(); ?>

</body>
</html>