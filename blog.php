<?php
// template name:New Blog
get_header( 'has-img' );
$page_id = get_queried_object_id();

?>

<main>
    <section>
        <div class="grid-container">
            <span class="mobile-headline"><h1>Blog</h1> </span>
        </div>
        <article class="grid-container">
            <?php
            $featured_story_type    = get_field('featured_story_type',  $page_id);
            $featured_story_title   = get_field('featured_story_title', $page_id);
            $featured_story_post    = get_field('featured_story_post',  $page_id);
            ?>
            <h2><?php echo $featured_story_title; ?></h2>

            <?php
            if ($featured_story_type == 'automatically') :
                $featured_story = new WP_Query(array(
                   'category_name'      => 'featured-story',
                    'posts_per_page'    => 1
                ));
                while ($featured_story->have_posts()) : $featured_story->the_post(); ?>
                    <div class="col <?php the_ID() ?>" >
                        <img src="<?php the_post_thumbnail_url('large') ?>"/>
                    </div>
                    <div class="col" style="<?php if(!has_post_thumbnail()) {echo "width:100%!important;position:static!important";} ?>">
                        <h2><?php the_title() ?></h2>
                        <p><?php echo wp_trim_words(get_the_content(),30,'...') ?></p>
                        <p>
                            <a href="<?php the_permalink() ?>" class="btn">Continue Reading</a>
                        </p>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
            <?php else:
                foreach ($featured_story_post as $post) : setup_postdata($post); ?>
                    <div class="col">
                        <img src="<?php the_post_thumbnail_url('large') ?>"/>
                    </div>
                    <div class="col" style="<?php if(!has_post_thumbnail()) {echo "width:100%!important;position:static!important";} ?>">
                        <h2><?php the_title() ?></h2>
                        <p><?php echo wp_trim_words(get_the_content(),30,'...') ?></p>
                        <p>
                            <a href="<?php the_permalink() ?>" class="btn">Continue Reading</a>
                        </p>
                    </div>
            <?php endforeach; wp_reset_postdata(); endif;  ?>
        </article>
    </section>
    <section>
	    <?php
	    $other_recent_news_title    = get_field('other_recent_news_title', $page_id);
	    $other_recent_news_type     = get_field('other_recent_news_type', $page_id);
	    $other_recent_news_posts    = get_field('other_recent_news_posts', $page_id);
	    ?>
        <div class="grid-container">
            <h2><?php echo $other_recent_news_title; ?></h2>
            <ul>
                
                <?php $cats =  get_categories([
                    'parent'    => 0,
                    'hide_empty' => false,
                    'exclude'   => array(304,1)
                ]);
                foreach ($cats as $cat) : ?>
                    <li class="cat-item cat-item-<?php echo $cat->term_id ?>">
                        <a href="<?php echo get_category_link($cat->term_id) ?>" ><?php echo $cat->name; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php if($other_recent_news_type == 'automatically') :
                $blog_news = new WP_Query(array(
                   'posts_per_page' => 6,
                   'post_types'  => 'post'
                ));
                $counter = 1 ;
                while ($blog_news->have_posts()) : $blog_news->the_post(); ?>
            <div class="col">
                <div class="img">
                    <a href="<?php the_permalink() ?>">
                        <?php if(has_post_thumbnail(  )) : ?>
                            <img src="<?php the_post_thumbnail_url('large') ?>"	 />
                        <?php else: ?>
                            <img style="max-height: 264px;" src="<?php echo get_template_directory_uri() . '/img/no-image.gif' ?>" />
                        <?php endif; ?>
                    </a>
                    <p>
                        <a style="color:#fff;" href="<?php echo get_category_link( get_the_category()[0] ) ?>"><?php echo get_the_category()[0]->name; ?></a>
                    </p>
                </div>
                <p class="date">
                   <?php the_time('M d, Y') ?>
                </p><h3><?php the_title() ?></h3>
                <p>
                    <a href="<?php the_permalink() ?>">Continue Reading</a>
                </p>
            </div>
                <?php if (wp_is_mobile()  && $counter % 2 == 0){echo '<div></div>';} ?>
                <?php $counter++; endwhile; wp_reset_query(); else:
                    $counter = 1 ;
                    foreach ($other_recent_news_posts as $post) : setup_postdata($post); ?>
                        <div class="col">
                            <div class="img">
                                <a href="<?php the_permalink() ?>">
                                    <?php if(has_post_thumbnail(  )) : ?>
                                        <img src="<?php the_post_thumbnail_url('large') ?>"	 />
                                    <?php else: ?>
                                        <img src="<?php echo get_template_directory_uri() . '/img/no-image.gif' ?>" />
                                    <?php endif; ?>
                                </a>
                                <p>
				                    <a href="<?php echo get_category_link( get_the_category()[0] ) ?>"><?php echo get_the_category()[0]->name; ?></a>
                                </p>
                            </div>
                            <p class="date">
			                    <?php the_time('M d, Y') ?>
                            </p><h3><?php the_title() ?></h3>
                            <p>
                                <a href="<?php the_permalink() ?>">Continue Reading</a>
                            </p>
                        </div>
            <?php if (wp_is_mobile()  && $counter % 2 == 0){echo '<div></div>';} ?>
            <?php $counter++ ;endforeach; endif; ?>

        </div>
    </section>
</main>

</main>
	</body>
</html>

<?php get_footer() ?>